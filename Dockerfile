#Get Node Image
FROM node:16

#Create app directory - this will be the working directory
WORKDIR /usr/src/app 

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install

# Bundle app source code inside the Docker Image
COPY . .

EXPOSE 3000
CMD [ "npm", "start" ]