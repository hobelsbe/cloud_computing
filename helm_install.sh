#!/bin/bash
echo startet minikube mit verbindung zu docker
#minikube start --driver=docker && minikube -p minikube docker-env && eval $(minikube docker-env)
#Windows commands
minikube start --driver=docker 
minikube -p minikube docker-env
minikube -p minikube docker-env --shell powershell | Invoke-Expression

# buildet docker images im minikube
docker compose build
docker compose up
helm uninstall cloudcomputingchart
helm install cloudcomputingchart cloud_computing_helm/ --values cloud_computing_helm/values.yaml

#check kubernetes pods
kubectl get pods
