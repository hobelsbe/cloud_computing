//node packages
const express = require('express');
const app = express();
const http = require('http');
const https = require('https'); 
const fs = require('fs');
const { mongo, default: mongoose, MongooseError } = require('mongoose');
//http Server
const server = http.createServer(app);

//https Server
/* const server = https.createServer({
  key: fs.readFileSync(__dirname + '/nginx/ssl/key.pem'),
  cert: fs.readFileSync(__dirname + '/nginx/ssl/cert.pem'),
  }, app);
   */

const { Server } = require("socket.io");
const io = new Server(server, {
  cors: {
    methods: ["GET", "POST"],
    transports: ['websocket', 'polling'],
    credentials: true
  }
});
const bodyParser = require('body-parser');
const registerRoute = require('./routes/register');
const loginRoute = require('./routes/login');
const activeUsers = new Map();
const authenticationRoute = require('./routes/auth');
const { response } = require('express');
const { resolve } = require('path');
const User = require('./models/user');
var port = process.env.PORT || 3000;
const redis = require('socket.io-redis');
const { createClient } = require('redis');
const { createAdapter } = require('@socket.io/redis-adapter')
var cors = require('cors');
/* 
const pubClient = createClient({ host: 'redis-server', port: 6379});
const subClient = pubClient.duplicate();

io.adapter(createAdapter(pubClient, subClient)); */
app.use(cors());

(async () => {
  pubClient = createClient({ url: "redis://redis-server:6379" });
  await pubClient.connect();
  subClient = pubClient.duplicate();
  io.adapter(createAdapter(pubClient, subClient));
})();


//DB Connection
//mongoose.connect("mongodb://mongodb:27017/cloudComputing");

const connectWithRetry = () => {
  let mongoConnectionString = 'mongodb://' + process.env.ADMIN_USER + ':' + process.env.ADMIN_PASSWORD + '@mongodb:27017/cloudComputing';
  mongoose
    .connect(mongoConnectionString, {useNewUrlParser: true})
    .then(() => console.log("successfully connected to DB"))
    .then(serverListen())
    .catch((e) => {
      console.log(e);
      setTimeout(connectWithRetry, 5000);
    });
};

connectWithRetry();

//Prometheus
const client = require('prom-client');

// Create a Registry to register the metrics
const register = new client.Registry();
client.collectDefaultMetrics({register});

/* const clientConnectionCounter = new client.Counter({
  name: 'node_user_connections_count',
  help: 'The total number of client connections'
}); */


// Declaring custom metrics to monitor the chatserver
// These metrics will monitor the system during runtime
// Prometheus will scrape these metrics and provide them to Gafana
const messageCounterIn = new client.Counter({
  name: 'node_message_counter_in',
  help: 'Counts the amount of incoming messages from clients to server'
});

const messageCounterOut = new client.Counter({
  name: 'node_message_counter_out',
  help: 'Counts the amount of outgoing messages from server to clients'
});

const messageAll = new client.Counter({
  name: 'node_message_counter_All',
  help: 'Counts the amount of times message to all users is used'
});

const messagePrivate = new client.Counter({
  name: 'node_message_counter_Private',
  help: 'Counts the amount of times private message is used'
});

const messageGroup = new client.Counter({
  name: 'node_message_counter_Group',
  help: 'Counts the amount of times group message is used'
});
    
const clientActiveUsers = new client.Gauge({
  name: 'node_active_users',
  help: 'Active Users'
    });

const successfullLogIns = new client.Counter({
  name: 'node_successful_logins',
  help: 'Successfull LogIns total'
  });
global.SUCCESSFULLLOG = successfullLogIns;

const notSuccessfullLogIns = new client.Counter({
  name: 'node_not_successful_logins',
  help: 'Not successfull LogIns total'
    });
global.NOTSUCCESSFULLLOG = notSuccessfullLogIns;

const audioFileCounter = new client.Counter({
  name: 'node_audiofile_counter',
  help: 'Counts the amount of times an audio file was sent'
});

const videoFileCounter = new client.Counter({
  name: 'node_videofile_counter',
  help: 'Counts the amount of times a video file was sent'
});

const imageFileCounter = new client.Counter({
  name: 'node_imagefile_counter',
  help: 'Counts the amount of times an image file was sent'
});

const totalClientsInDB = new client.Gauge({
  name: 'mongodb_total_amount_of_users_in_mongodb',
  help: 'Total amount of users in mongoDB'
    });
global.TOTALCLIENTSINDB = totalClientsInDB;

// Register the metrics, to be collected in Prometheus database
register.registerMetric(messageCounterIn);
register.registerMetric(messageCounterOut);
register.registerMetric(messageAll);
register.registerMetric(messagePrivate);
register.registerMetric(messageGroup);
register.registerMetric(successfullLogIns);
register.registerMetric(notSuccessfullLogIns);
register.registerMetric(clientActiveUsers);
register.registerMetric(audioFileCounter);
register.registerMetric(videoFileCounter);
register.registerMetric(imageFileCounter);
register.registerMetric(totalClientsInDB);

//MongoDB Monitoring
var totalUsersinDB = User.find();
totalUsersinDB.count(function (err, count) {
    if (err) console.log(err)
    else totalClientsInDB.set(count);
});


app.use(bodyParser.json());
//Routes
app.use('/register', registerRoute);
app.use('/login', loginRoute);
app.use('/authenticate', authenticationRoute)

//Views
app.get('/', (req, res) => {
  console.log("Verbunden mit: "  + process.env.NAME);
  res.sendFile(__dirname + '/views/start.html');
}); 

app.get('/chatroom', (req, res) => {
  res.sendFile(__dirname + '/views/index.html');
});

app.get('/register', (req, res) => {
  res.sendFile(__dirname + '/views/register.html');
}); 

app.get('/login', (req, res) => {
  res.sendFile(__dirname + '/views/login.html');
});

/**
* Summary:
* Function for handling updating the List of users eho are currently online.
* 
*
* Description:
* Function will call addUserToActiveUsers() when a clients connects to the chatroom.
* Function will call deleteUserFromActiveUsers() when a clients diconnects from the chatroom.
*
* 
* @param {Object} response
* @triggers addUserToActiveUsers()
* @triggers deleteUserFromActiveUsers()
* @listens  socket.on() 'userLogin' event
* @listens  socket.on() 'disconnect' event
*/
io.on('connection', (socket) => {
  
  socket.on('userLogin', async function(response) {
    await addUserToActiveUsers(response, socket);
    clientActiveUsers.set(activeUsers.size);
  });
  
  socket.on('disconnect', async function(response) {
    await deleteUserFromActiveUsers(response, socket);
  });

  
});


/**
* Summary:
* Function to add a user to map of users, who are currently online.
*
* Description:
* Function will take the username from response and append it together with the socketID to map activeUsers.
* after appending, io.emit() will send the updated userlist to all users who are currently online.
*
* @param {socket} socket
* @param {Object} response
* @triggers io.emit() 'updateUserlist' event
* @listens  socket.on() 'userLogin' event
*/
async function addUserToActiveUsers(response, socket) {

  return new Promise(resolve => {

    activeUsers.set(socket.id, response.name);
    clientActiveUsers.set(activeUsers.size);

    var userArr = Array.from(activeUsers.values());

    io.emit('updateUserlist', {
      userList : userArr
    });
  });
}


/**
* Summary:
* Function to delete a user from the map of users, who are currently online.
*
* Description:
* Function will take the socketID from the socket, of the client, who is disconneting.
* The socketID and its value (username) will be deleted from map activeUsers.
* After deleting the socketID and username io.emit() is called and the upodated userList is sent to all users who are currently online.
*
* @param {socket} socket
* @param {Object} response
* @triggers io.emit() 'updateUserlist' event
* @listens  socket.on() 'disconnect' event
*/
async function deleteUserFromActiveUsers(response, socket) {

  return new Promise(resolve => {
    activeUsers.delete(socket.id);
    clientActiveUsers.set(activeUsers.size);

    var userArr = Array.from(activeUsers.values());
    io.emit('updateUserlist', {
      userList : userArr
    });
  });
}


/**
* Summary:
* Function for forwarding chat messages.
*
* Description:
* Function will take the username of the sender and the message from the response.
* Server will append the current time and call io.emit() 'chat message' event.
* Json with the message, username of sender and current time is sent to all users who are currently online.
*
* 
* @param {Object} response
* @triggers io.emit() 'chat message' event
* @listens  socket.on() 'chat message' event
*/
io.on('connection', (socket) => {
  socket.on('chat message', (response) =>{

      messageCounterIn.inc(1);
      messageAll.inc(1);

      var msg = response.msg;
      var name = response.name;
      var dateTime = new Date().toTimeString();
      console.log("Received Message: " + msg);
      messageCounterOut.inc(activeUsers.size);

     io.emit('chat message', {
        msg : msg,
        name : name,
        dateTime: dateTime
      });
  })
});

/**
* Summary:
* Function for forwarding private chat messages.
* 
*
* Description:
* Function checks whether the receiver is online at the moment.
* If the receiver is online, the private message is sent to him/her.
* Function will take the username of the sender, the private message, the name of the receiver  from the response.
* Server will append the current time and call io.emit() 'chat message' event.
* 
*
* 
* @param {Object} response
* @triggers io.emit() 'private message' event
* @listens  socket.on() 'private message' event
*/
io.on('connection', (socket) => {
  socket.on('private message', (response) => {

    messageCounterIn.inc(1);
    messagePrivate.inc(1);

    var private_msg = response.msg;
    var receiver = response.receiver;
    var sender = response.name;
    var array = Array.from(activeUsers);
    var dateTime = new Date().toTimeString();

    for(var i = 0; i<array.length; i++) {
      if(array[i][1] == receiver){

        messageCounterOut.inc(2);

        io.emit(receiver, {
          msg : private_msg,
          sender : sender,
          receiver: receiver,
          dateTime: dateTime
          })

        io.emit(sender, {
          msg : private_msg,
          sender : sender,
          receiver: receiver,
          dateTime: dateTime
        })
      return 0;
        }
      }
    io.emit(sender, {
      status: 401
    })
    })

/**
* Summary:
* Function for forwarding group chat messages.
* 
*
* Description:
* Function checks whether the receivers are online at the moment.
* If the receivers are online, the group message is sent to them.
* Function will take the username of the sender, the private message, the name of the receiver  from the response.
* Server will append the current time and call io.emit() 'chat message' event.
* 
*
* 
* @param {Object} response
* @triggers io.emit() 'groupMessage' event
* @listens  socket.on() 'groupMessage' event
*/
  socket.on('groupMessage', (response) => {
    
    messageCounterIn.inc(1);
    messageGroup.inc(1);
    
    let sender = response.name;
    let receivers = response.receivers;
    let groupMsg = response.groupMsg;
    var dateTime = new Date().toTimeString();

    console.log("receivers", receivers[0]);
    console.log("groupmsg", groupMsg);

    let a = activeUsers.values;

    console.log("sender", sender);

    for (let i = 0; i<receivers.length; i++) {

      messageCounterOut.inc(1);

      io.emit(receivers[i].trim(), {
        msg: groupMsg,
        sender: sender,
        receiver: receivers,
        dateTime: dateTime
      })
    }

    messageCounterOut.inc(1);

    io.emit(sender, {
      msg: groupMsg,
      sender: sender,
      receiver: receivers,
      dateTime: dateTime
    })
  })
})


//Logs message from Socket to Console
io.on('connection', (socket) => {
  socket.on('chat message', (response) => {
    var msg = response.msg;
  });
});


/**
* Summary:
* Function for sending image files
* 
*
* Description:
* By triggering the 'sendImage' Event, the function emits the given files url to client
*
* @listens  socket.on() 'sendImage' event
*/
io.on("connection", (socket) => {
  socket.on('sendImage', (file, callback) => {
    imageFileCounter.inc(1);
    io.emit('image', file);
  });
});

/**
* Summary:
* Function for sending video files
* 
*
* Description:
* By triggering the 'sendVideo' Event, the function emits the given files url to client
*
* @listens  socket.on() 'sendVideo' event
*/
io.on("connection", (socket) => {
  socket.on('sendVideo', (file, callback) => {
    videoFileCounter.inc(1);
    io.emit('video', file);
  });
});

/**
* Summary:
* Function for sending audio files
* 
*
* Description:
* By triggering the 'sendAudio' Event, the function emits the given files url to client
*
* @listens  socket.on() 'sendAudio' event
*/
io.on("connection", (socket) => {
  socket.on('sendAudio', (file, callback) => {
    audioFileCounter.inc(1);
    io.emit('audio', file);
  });
});


//http Server
/* server.listen(3000, () => {
  console.log('listening on Port :3000');
});  */

// Metrics endpoint
app.get('/metrics', async (req, res) => {
  res.setHeader('Content-Type', register.contentType);
  res.send(await register.metrics());
});

/* function serverListen() {
  server.listen(5000, () => console.log('Server on Port 5000')); 
} */

//http Server

function serverListen() {
  server.listen(process.env.PORT, function () {

    console.log('Server listening at port %d', port);
    console.log('Hello, I\'m ', process.env.NAME, ' how can I help?');

  });
}


module.exports = {activeUsers};
