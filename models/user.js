    /**
    * Summary:
    * Schema for MongoDB wich defines the shape of the documents within the collection
    *
    */

const mongoose = require('mongoose');
const schema = mongoose.Schema;

const userSchema = new schema({
    name: {
        type: String
    },
    password: {
        type: String
    }
}, {timestamps: true})

const User = mongoose.model('User', userSchema);
module.exports = User