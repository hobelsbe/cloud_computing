const User = require('../models/user');
const http = require('http');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router()
const bodyParser = require('body-parser');
const { verify } = require('crypto');
require('dotenv').config();

/**
* Summary:
* Handles authorization of client.
*
* Description:
* The function receives clients webtoken over the authorization part of the request header. By using JWT as technology, we can
* verify the token of the client. The signature of the client has to be valid, otherwise the function sends 401 as status code. 
* JWT provides a assymetric encryption process.
*
*
* @listens fetch '/register' from client
*
* @param {Object}   res         Servers response for HTTP body
* @param {Object}   req         Clients requests http body
*
*
* @return {HTTP statusCode} 200
* @return {HTTP statusCode} 401
*/
router.get('/', async(req, res) => {
        
        authHeader = req.headers.authorization;

        try{
            jwt.verify(authHeader, process.env.ACCESS_TOKEN_SECRET);
            res.sendStatus(200);
        } catch(err){
            console.log(err);
            res.sendStatus(401);
        }
       
});

module.exports = router;