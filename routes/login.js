const User = require('../models/user');
const http = require('http');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router()
const bodyParser = require('body-parser');
require('dotenv').config();

/**
* Summary:
* Function to verify the Login of a client.
*
* Description:
* Username and password of client get checked, whether they exist in the database.
* Function will check, whether username and password from request are the same, as in the database. 
* If successful, a JWT accessToke is created and statuscode 200 and the Accesstoke are sent to client.
* If not successful, statuscode 401 is sent to client.
* Uses bcrypt library.
*
* @listens fetch to"/login" from client
*
* @param {object}   req           Json Body from Client.
* @return {json} res            Response from server.
*/
router.post('/', async(req, res) => {
    var name = req.body.name;
    var password = req.body.password;
    console.log(req.body);
    User.findOne({name: name})
    .then(user => {
        if(user){
            bcrypt.compare(password, user.password, function (err, result) {
                if(err){
                    NOTSUCCESSFULLLOG.inc(1);
                    res.json(err);
                }
                if(result){
                    SUCCESSFULLLOG.inc(1);
                    const user = { name: name }; 
                    const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
                    res.json({
                        statusCode: '200',
                        accessToken: accessToken,
                    })
                }else{
                    NOTSUCCESSFULLLOG.inc(1);
                    res.status(401);
                }
            })
        }else{
            NOTSUCCESSFULLLOG.inc(1);
            res.status(401);
        }
    })
});



module.exports = router;