const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router()
const bodyParser = require('body-parser');

/**
* Summary:
* Handles registration of client.
*
* Description:
* Function handles registration of client.
* Uses bcrypt library.
* Checks, whether username already exists or not.
* If username is not taken yet, the username und the hashed password are saved to the database using mongoDB and 200 is sent to client.
* if not successful 401 is sent to client.
*  
*
*
* @listens fetch '/register' from client
*
* @param {Object}   res         Servers response for HTTP body
* @param {Object}   req         Clients requests http body
*
*
* @return {HTTP statusCode} 200
* @return {HTTP statusCode} 401
*/
router.post('/', async(req, res) => {
    console.log(req.body);
    bcrypt.hash(req.body.password, 10, function(err, hashedPass){
        if(err){
            res.json({
                error: err
            })
        }
        let user = new User ({
            name: req.body.name, 
            password: hashedPass
        })
  
        var query = User.find({
            "name" : req.body.name
        });

        query.count(function(err, count) { 
            if (count !== 0) {
                res.sendStatus(401);
            }
            else {
                user.save()
                .then(user => {
                    res.sendStatus(200);
                })
                .catch(error => {
                    res.json({
                        message: 'Error occured'
                    })
                })
                var totalUsersinDB = User.find();
                totalUsersinDB.count(function (err, count) {
                    if (err) console.log(err)
                    else TOTALCLIENTSINDB.set(count);
                });
            }
        })
    })
});



module.exports = router;